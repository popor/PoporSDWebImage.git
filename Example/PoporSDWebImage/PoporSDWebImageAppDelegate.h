//
//  PoporSDWebImageAppDelegate.h
//  PoporSDWebImage
//
//  Created by popor on 06/22/2018.
//  Copyright (c) 2018 popor. All rights reserved.
//

@import UIKit;

@interface PoporSDWebImageAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
